let http = require("http");

http.createServer(function (req, res) {

  // The method GET means that we will be retrieving or reading information.
  if (req.url == "/items" && req.method == "GET") {
    res.writeHead(200, { "content-type": "text/plain" });
    res.end("data retrieved from database")
  }

  // The method POST means that we will be adding or creating information
  if (req.url == "/Addcourse" && req.method == "POST") {
    res.writeHead(200, { "content-type": "text/plain" });
    res.end("Data to be sent to the database")
  }

}).listen(3000);

console.log(`system is now running localhost 3000`);