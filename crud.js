const http = require('http');
const port = 3000;

const server = http.createServer((req, res) => {
  if (req.url == "/Homepage") {
    res.writeHead(200, { "Content-type": "text/plain" })
    res.end("Welcome to booking system")
  } else if (req.url == "/Profile") {
    res.writeHead(200, { "Content-type": "text/plain" })
    res.end("Welcome to your profile")
  } else if (req.url == "/Courses") {
    res.writeHead(200, { "Content-type": "text/plain" })
    res.end("Here's our courses available")
  } else if (req.url == "/Addcourse") {
    res.writeHead(200, { "Content-type": "text/plain" })
    res.end("Add courses to our resources")
  } else {
    res.writeHead(200, { "Content-type": "text/plain" })
    res.end("404: Page di nakita hehe.")
  }
});

server.listen(port);

console.log(`EDIT: Server now accesible at localhost:${port}`);